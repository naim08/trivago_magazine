import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';

import { MetaService } from 'ng2-meta';

import { BackendMagazineService } from '../../../../shared/services/magazine.service';

@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss']
})
export class DetailPage implements OnInit {

  ratingForm: FormGroup;
  article: object;
  successfulFormSubmission: boolean;
  showForm: boolean;
  formSubmitted: boolean;
  rating: number;
  model: object;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private metaService: MetaService,
    private backendMagazineService: BackendMagazineService) {
    
    this.formSubmitted = false;
    this.successfulFormSubmission = false;
    this.showForm = false;
    this.rating = 0;
    this.model = {
      'userId': localStorage.getItem("userId"),
      'articleId': null,
      'rating': null
    };

    // Create rating form group
    this.ratingForm = this.formBuilder.group({
      rating: ['', [Validators.required, Validators.max(5), Validators.min(1)]],
    });
  }

  ngOnInit() {
    const slug = this.route.snapshot.paramMap.get("slug");

    // Get article by slug
    this.backendMagazineService.getArticle(slug)
      .subscribe(article => {
        this.article = article;
        this.model['articleId'] = article['id'];

        // Set meta data
        this.metaService.setTitle(article['seo_meta']['title']);
        this.metaService.setTag('description', article['seo_meta']['opengraph_description']);
        this.metaService.setTag('og:image', article['seo_meta']['opengraph_image']);

        // Get rating for current user
        this.backendMagazineService.getRatingForUser(this.model['articleId'], this.model['userId'])
          .subscribe((ratings: any[]) => {

            if (ratings.length == 0) {
              this.showForm = true;
            }
          }, error => { console.log(error); });

        // Update rating
        this.update_rating();
      });
  }

  onSubmit() {
    this.formSubmitted = true;

    // Return if form is invalid
    if (!this.ratingForm.valid) { 
      return; 
    }

    // Map form data to model data 
    Object.assign(this.model, this.ratingForm.value);

    // Create rating
    this.backendMagazineService.createRating(this.model)
      .subscribe(rating => {
        this.successfulFormSubmission = true;

        // Update rating
        this.update_rating();
      }, error => { console.log(error); });
  }

  update_rating() {
    this.backendMagazineService.getRating(this.model['articleId'])
      .subscribe((ratings: any[]) => {
        var total = 0;

        for (var i = 0; i < ratings.length; i++) {
          if(ratings[i].hasOwnProperty('rating')){
            total += ratings[i]['rating'];
          }
        }

        this.rating = Number((total/ratings.length).toFixed(2));
      }, error => { console.log(error); });
  }

}
