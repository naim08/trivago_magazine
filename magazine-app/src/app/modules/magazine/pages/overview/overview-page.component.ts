import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { BackendMagazineService } from '../../../../shared/services/magazine.service';

@Component({
  selector: 'app-overview-page',
  templateUrl: './overview-page.component.html',
  styleUrls: ['./overview-page.component.scss']
})
export class OverviewPage implements OnInit {

  articleList$: Observable<any[]>;

  constructor(private backendMagazineService: BackendMagazineService) {}

  ngOnInit() {
    // Load article list
    this.loadArticleList();
  }

  loadArticleList() {

    // Get all articles
    this.articleList$ = this.backendMagazineService.getArticleList();
  }

}
