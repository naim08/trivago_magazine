import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MetaGuard } from 'ng2-meta';

import { OverviewPage } from './pages/overview/overview-page.component';
import { DetailPage } from './pages/detail/detail-page.component';

const routes: Routes = [
	{ 
		path: "", 
		component: OverviewPage,
		canActivate: [MetaGuard],
    data: {
      meta: {
        title: 'Trivago magazine',
        description: 'Magazine and much more'
      }
    }
	},
  { 
  	path: 'detail/:slug', 
  	component: DetailPage 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MagazineRoutingModule { }
