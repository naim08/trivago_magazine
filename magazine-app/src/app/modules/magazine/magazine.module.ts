import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MagazineRoutingModule } from './magazine-routing.module';
import { OverviewPage } from './pages/overview/overview-page.component';
import { DetailPage } from './pages/detail/detail-page.component';

@NgModule({
  declarations: [OverviewPage, DetailPage],
  imports: [
  	FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MagazineRoutingModule
  ]
})
export class MagazineModule { }
