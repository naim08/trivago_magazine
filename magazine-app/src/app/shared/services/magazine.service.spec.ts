import { TestBed } from '@angular/core/testing';

import { BackendMagazineService } from './magazine.service';

describe('BackendMagazineService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BackendMagazineService = TestBed.get(BackendMagazineService);
    expect(service).toBeTruthy();
  });
});
