import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { BackendService } from './backend.service';

@Injectable({
  providedIn: 'root'
})
export class BackendMagazineService {

  constructor(private backendService: BackendService) { }

  getArticle (slug: string): Observable<object> {
    return this.backendService.sendRequestGetBySlug(
    		'http://trivago-magazine-work-sample-server.s3-website.eu-central-1.amazonaws.com/', 
    		slug
    	);
  }

  getArticleList (): Observable<object[]> {
    return this.backendService.sendRequestGet(
    		'http://trivago-magazine-work-sample-server.s3-website.eu-central-1.amazonaws.com/latest_posts.json'
    	);
  }

  getRating (articleId): Observable<object> {
    return this.backendService.sendRequestGet('http://localhost:3000/ratings/'+articleId);
  }

  getRatingForUser (articleId, userId): Observable<object> {
    return this.backendService.sendRequestGet('http://localhost:3000/ratings/'+articleId+'/'+userId);
  }

  createRating (article): Observable<object> {
    return this.backendService.sendRequestPost('http://localhost:3000/ratings', article);
  }
}
