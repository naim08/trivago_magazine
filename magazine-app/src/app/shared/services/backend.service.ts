import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

	constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
   
      // Show error
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
   
      // Let the app keep running by throwing an error to show custom message on component.
      return throwError(error);
    };
  }

  sendRequestGet (url: string): Observable<any[]> {
    let completeUrl = `${url}`;

    return this.http.get<any[]>(completeUrl).pipe(
      tap(_ => console.log(`fetched all ${url}`)),
      catchError(this.handleError<any>(`get objects ${url}`))
    );
  }

  sendRequestGetBySlug(url: string, slug: string): Observable<any> {
    let completeUrl = `${url}${slug}.json`;

    return this.http.get<any>(completeUrl).pipe(
      tap(_ => console.log(`fetched by slug=${slug}`)),
      catchError(this.handleError<any>(`get object ${slug}`))
    );
  }

  sendRequestPost(url: string, data: object): Observable<any> {
    let completeUrl = `${url}`;

    return this.http.post<any>(completeUrl, data).pipe(
      tap(_ => console.log(`created ${url}`)),
      catchError(this.handleError<any>(`create object ${url}`))
    );
  }
}
