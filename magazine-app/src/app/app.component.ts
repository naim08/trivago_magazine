import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'trivago-magazine';

  constructor() {}

  ngOnInit() {
  	let userId = localStorage.getItem("userId");

    // Set userId if not set
    if (userId === null) {
      localStorage.setItem("userId", Math.random().toString(36).replace('0.', ''));
    }
  }
}
