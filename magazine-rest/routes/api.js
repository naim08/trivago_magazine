const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

// MongoDB URL from the docker-compose file
const dbHost = 'mongodb://database/trivago_magazine';

// Connect to mongodb
mongoose.connect(dbHost);

// Create mongoose schema
const ratingSchema = new mongoose.Schema({
  userId: String,
  articleId: Number,
  rating: Number
});

// Create mongoose model
const Rating = mongoose.model('Rating', ratingSchema);

// Create rating
router.post('/ratings', (req, res) => {
    let rating = new Rating({
        userId: req.body.userId,
        articleId: req.body.articleId,
        rating: req.body.rating
    });

    rating.save(error => {
        if (error) res.status(500).send(error);

        res.status(201).json({
            message: 'Rating created successfully'
        });
    });
});

// Get all ratings for a article and user
router.get('/ratings/:articleId/:userId*?', (req, res) => {
	let search_query = {
		articleId: req.params.articleId
	}
	if (req.params.userId) {
		search_query['userId'] = req.params.userId;
	}

    Rating.find(search_query, (err, articles) => {
        if (err) res.status(500).send(error)

        res.status(200).json(articles);
    });
});

// Export
module.exports = router;