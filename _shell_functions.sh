# Functions to get into shell of the machines
nd_getshell_angular () { 
	docker exec -it trivago_magazine-angular ash 
}

nd_getshell_node () { 
	docker exec -it trivago_magazine-node bash 
}

#
# Angular
#
angular_ng () { 
	docker exec -it trivago_magazine-angular ng $*
}

angular_ng_serve () { 
	docker exec -it trivago_magazine-angular ng serve --host 0.0.0.0 --disable-host-check
}

angular_npm () { 
	docker exec -it trivago_magazine-angular npm $*
}

#
# Node
#
node_npm () {
	docker exec -it trivago_magazine-node npm $*
}